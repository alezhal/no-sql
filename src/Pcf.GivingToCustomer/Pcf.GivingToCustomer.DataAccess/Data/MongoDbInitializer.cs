﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data;
using Pcf.GivingToCustomer.DataAccess.Mongo;
using Pcf.GivingToCustomer.Integration.Gateways;

namespace Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {
        private readonly IMongoCollection<Customer> _customers;
        private readonly IMongoCollection<Preference> _preferences;

        public MongoDbInitializer(IMongoDbSettings settings, PreferencesGateway preferencesGateway)
        {
            var database = new MongoClient(settings.ConnectionString).GetDatabase(settings.DbName);
            _customers = database.GetCollection<Customer>($"{nameof(Customer)}s");
            _preferences = database.GetCollection<Preference>($"{nameof(Preference)}s");
        }

        public void InitializeDb()
        {
            _customers.DeleteMany(r => true);
            _preferences.DeleteMany(r => true);
            _customers.InsertMany(FakeDataFactory.Customers);
            _preferences.InsertMany(FakeDataFactory.Preferences);
        }
    }
}
