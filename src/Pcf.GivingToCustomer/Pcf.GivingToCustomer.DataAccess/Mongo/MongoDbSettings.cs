﻿namespace Pcf.GivingToCustomer.DataAccess.Mongo
{
    public class MongoDbSettings: IMongoDbSettings
    {
        public string ConnectionString { get; set; }
        public string DbName { get; set; }
    }
}
