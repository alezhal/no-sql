﻿namespace Pcf.GivingToCustomer.DataAccess.Mongo
{
    public interface IMongoDbSettings
    {
        string ConnectionString { get; set; }
        string DbName { get; set; }
    }
}
