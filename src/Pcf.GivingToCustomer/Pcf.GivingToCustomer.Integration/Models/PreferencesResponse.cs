﻿using System;

namespace Pcf.GivingToCustomer.Integration.Models
{
    public class PreferencesResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
