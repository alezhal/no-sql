﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Pcf.GivingToCustomer.Integration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Pcf.GivingToCustomer.Integration.Gateways
{
    public class PreferencesGateway
    {
        private readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;

        public PreferencesGateway(HttpClient httpClient, IConfiguration configuration) =>
            (_httpClient, _configuration) = (httpClient, configuration);


        public async Task<List<PreferencesResponse>> GetPreferences(CancellationToken cts = default)
        {
            var response = await _httpClient.GetAsync(_configuration["IntegrationSettings:GetAllPreferencesUri"]);
            if (response.IsSuccessStatusCode)
            {
                var jsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<PreferencesResponse>>(jsonString);
            }
            else
            {
                string msg = await response.Content.ReadAsStringAsync();
                throw new System.Exception(msg);
            }
        }

        public async Task<IEnumerable<PreferencesResponse>> GetRangeByIdsAsync(List<Guid> ids, CancellationToken cts = default)
        {
            var data = ids.Select(d => $"ids={d}");
            var query = string.Join("&", data);
            var uri = $"{_configuration["IntegrationSettings:GetRangePreferencesByIdUri"]}{query}";

            var response = await _httpClient.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var jsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<PreferencesResponse>>(jsonString);
            }
            else
            {
                string msg = await response.Content.ReadAsStringAsync();
                throw new System.Exception(msg);
            }
        }

        public async Task<PreferencesResponse> GetByIdAsync(Guid id)
        {
            var uri = $"{_configuration["IntegrationSettings:GetPreferenceByIdUri"]}{id}";
            var response = await _httpClient.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var jsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<PreferencesResponse>(jsonString);
            }
            else
            {
                string msg = await response.Content.ReadAsStringAsync();
                throw new System.Exception(msg);
            }
        }

        public async Task<PreferencesResponse> PostAsync(PreferencesResponse preference)
        {
            var uri = $"{_configuration["IntegrationSettings:PostPreferenceUri"]}";
            string json = JsonConvert.SerializeObject(preference);
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync(uri, httpContent);

            if (response.IsSuccessStatusCode)
            {
                var jsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<PreferencesResponse>(jsonString);
            }
            else
            {
                string msg = await response.Content.ReadAsStringAsync();
                throw new System.Exception(msg);
            }
        }
    }
}
