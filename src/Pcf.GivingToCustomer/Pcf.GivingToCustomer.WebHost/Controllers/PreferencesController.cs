﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using Pcf.GivingToCustomer.Integration.Gateways;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения клиентов
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {
        private readonly PreferencesGateway _preferencesGateway;

        public PreferencesController(
            IRepository<Preference> preferencesRepository,
            PreferencesGateway preferencesGateway) =>
            _preferencesGateway = preferencesGateway;
        
        /// <summary>
        /// Получить список предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PreferenceResponse>>> GetPreferencesAsync()
        {
            #region Getting preferences from Redis cache
            var response = await _preferencesGateway.GetPreferences();
            #endregion

            return Ok(response);
        }
    }
}