﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using Pcf.GivingToCustomer.Integration.Gateways;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly PreferencesGateway _preferencesGateway;

        public CustomersController(
            IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository,
            IRepository<PromoCode> promocodeRepository,
            PreferencesGateway preferencesGateway) => 
            (_customerRepository, _preferenceRepository, _promocodeRepository, _preferencesGateway) = 
            (customerRepository, preferenceRepository, promocodeRepository, preferencesGateway);
        
        /// <summary>
        /// Получить список клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers =  await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            return Ok(response);
        }
        
        /// <summary>
        /// Получить клиента по id
        /// </summary>
        /// <param name="id">Id клиента, например <example>a6c8c6b1-4349-45b0-ab31-244740aaf0f0</example></param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer =  await _customerRepository.GetByIdAsync(id);

            #region Getting preferences from DB
            //var preferences = await _preferenceRepository.GetRangeByIdsAsync(customer.Preferences.Select(p => p.PreferenceId).ToList());
            #endregion

            #region Getting preferences from Redis cache
            var preferences = await _preferencesGateway.GetRangeByIdsAsync(customer.Preferences.Select(p => p.PreferenceId).ToList());
            #endregion

            customer.Preferences = preferences.Select(p => new CustomerPreference
            {
                Preference = new Preference
                {
                    Id = p.Id,
                    Name = p.Name
                },
                PreferenceId = p.Id
            }).ToList();

            var promocodes =
                await _promocodeRepository.GetRangeByIdsAsync(customer.PromoCodes.Select(p => p.PromoCode.Id).ToList());
            customer.PromoCodes = promocodes.Select(p => new PromoCodeCustomer
            {
                PromoCode = new PromoCode
                {
                    Id = p.Id,
                    BeginDate = p.BeginDate,
                    Code = p.Code,
                    ServiceInfo = p.ServiceInfo,
                    PartnerId = p.PartnerId,
                    EndDate = p.EndDate
                },
                PromoCodeId = p.Id
            }).ToList();

            var response = new CustomerResponse(customer);

            return Ok(response);
        }
        
        /// <summary>
        /// Создать нового клиента
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<CustomerResponse>> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            #region Getting preferences from DB //Получаем предпочтения из бд и сохраняем большой объект
            //var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);
            //Customer customer = CustomerMapper.MapFromModel(request, preferences);
            #endregion

            #region Getting preferences from Redis cache
            var preferences = await _preferencesGateway.GetRangeByIdsAsync(request.PreferenceIds);
            Customer customer = CustomerMapper.MapFromGateWayModel(request, preferences);
            #endregion
            
            await _customerRepository.AddAsync(customer);

            return CreatedAtAction(nameof(GetCustomerAsync), new {id = customer.Id}, customer.Id);
        }
        
        /// <summary>
        /// Обновить клиента
        /// </summary>
        /// <param name="id">Id клиента, например <example>a6c8c6b1-4349-45b0-ab31-244740aaf0f0</example></param>
        /// <param name="request">Данные запроса></param>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            
            if (customer == null)
                return NotFound();

            #region Getting preferences from DB
            //var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);
            //CustomerMapper.MapFromModel(request, preferences, customer);
            #endregion

            #region Getting preferences from Redis cache
            var preferences = await _preferencesGateway.GetRangeByIdsAsync(request.PreferenceIds);
            CustomerMapper.MapFromGateWayModel(request, preferences, customer);
            #endregion

            await _customerRepository.UpdateAsync(customer);

            return NoContent();
        }
        
        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <param name="id">Id клиента, например <example>a6c8c6b1-4349-45b0-ab31-244740aaf0f0</example></param>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            
            if (customer == null)
                return NotFound();

            await _customerRepository.DeleteAsync(customer);

            return NoContent();
        }
    }
}