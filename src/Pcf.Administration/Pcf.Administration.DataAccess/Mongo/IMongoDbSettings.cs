﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pcf.Administration.DataAccess.Mongo
{
    public interface IMongoDbSettings
    {
        string ConnectionString { get; set; }
        string DbName { get; set; }
    }
}
