﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.DataAccess.Data;
using Pcf.Administration.DataAccess.Mongo;

namespace Pcf.Administration.DataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {
        private readonly IMongoCollection<Role> _roles;
        private readonly IMongoCollection<Employee> _employees;

        public MongoDbInitializer(IMongoDbSettings settings)
        {
            var database = new MongoClient(settings.ConnectionString).GetDatabase(settings.DbName);
            _roles = database.GetCollection<Role>($"{nameof(Role)}s");
            _employees = database.GetCollection<Employee>($"{nameof(Employee)}s");
        }

        public void InitializeDb()
        {
            _roles.DeleteMany(r => true);
            _employees.DeleteMany(r => true);
            _roles.InsertMany(FakeDataFactory.Roles);
            _employees.InsertMany(FakeDataFactory.Employees);
        }
    }
}
