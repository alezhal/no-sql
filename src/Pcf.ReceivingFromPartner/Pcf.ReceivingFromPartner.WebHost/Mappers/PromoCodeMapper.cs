﻿using System;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Models;
using Pcf.ReceivingFromPartner.Integration.Models;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Mappers
{
    public class PromoCodeMapper
    {
        public static PromoCode MapFromModel(ReceivingPromoCodeRequest request, Preference preference, Partner partner)
        {
            var promocode = new PromoCode
            {
                PartnerId = partner.Id,
                Partner = partner,
                Code = request.PromoCode,
                ServiceInfo = request.ServiceInfo,
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(30),
                Preference = preference,
                PreferenceId = preference.Id,
                PartnerManagerId = request.PartnerManagerId
            };

            return promocode;
        }

        public static PromoCode MapFromGatewayModel(ReceivingPromoCodeRequest request, PreferencesResponse preference, Partner partner)
        {
            var promocode = new PromoCode
            {
                PartnerId = partner.Id,
                Partner = partner,
                Code = request.PromoCode,
                ServiceInfo = request.ServiceInfo,
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(30),
                PreferenceId = preference.Id,
                PartnerManagerId = request.PartnerManagerId,
                Preference = new Preference
                {
                    Id = preference.Id,
                    Name = preference.Name
                }
            };

            return promocode;
        }
    }
}
