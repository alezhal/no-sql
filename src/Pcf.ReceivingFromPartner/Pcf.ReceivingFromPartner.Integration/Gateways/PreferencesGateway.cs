﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Pcf.ReceivingFromPartner.Integration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Pcf.ReceivingFromPartner.Integration.Gateways
{
    public class PreferencesGateway
    {
        private readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;

        public PreferencesGateway(HttpClient httpClient, IConfiguration configuration) =>
            (_httpClient, _configuration) = (httpClient, configuration);


        public async Task<List<PreferencesResponse>> GetPreferences(CancellationToken cts = default)
        {
            var response = await _httpClient.GetAsync(_configuration["IntegrationSettings:GetAllPreferencesUri"]);
            if (response.IsSuccessStatusCode)
            {
                var jsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<PreferencesResponse>>(jsonString);
            }
            else
            {
                string msg = await response.Content.ReadAsStringAsync();
                throw new System.Exception(msg);
            }
        }

        public async Task<IEnumerable<PreferencesResponse>> GetRangeByIdsAsync(List<Guid> ids, CancellationToken cts = default)
        {
            //var dates = ids.Select(d => string.Format("ids={0}", UrlEncode(d));
            var dates = ids.Select(d => $"ids={d}");
            var query = string.Join("&", dates);
            var uri = $"{_configuration["IntegrationSettings:GetRangePreferencesByIdUri"]}{query}";

            var response = await _httpClient.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var jsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<PreferencesResponse>>(jsonString);
            }
            else
            {
                string msg = await response.Content.ReadAsStringAsync();
                throw new System.Exception(msg);
            }
        }

        public async Task<PreferencesResponse> GetByIdAsync(Guid id)
        {
            var uri = $"{_configuration["IntegrationSettings:GetPreferenceByIdUri"]}{id}";
            var response = await _httpClient.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var jsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<PreferencesResponse>(jsonString);
            }
            else
            {
                string msg = await response.Content.ReadAsStringAsync();
                throw new System.Exception(msg);
            }
        }
    }
}