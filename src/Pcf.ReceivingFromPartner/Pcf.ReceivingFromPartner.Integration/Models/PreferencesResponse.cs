﻿using System;

namespace Pcf.ReceivingFromPartner.Integration.Models
{
    public class PreferencesResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
