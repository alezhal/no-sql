﻿namespace Pcf.Preferences.Models
{
    public class PreferenceResponse
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}
