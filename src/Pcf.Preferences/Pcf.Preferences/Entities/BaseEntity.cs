﻿using System;

namespace Pcf.Preferences.Entities
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}
