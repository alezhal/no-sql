﻿namespace Pcf.Preferences.Entities
{
    public class Preference : BaseEntity
    {
        public string Name { get; set; }
    }
}
