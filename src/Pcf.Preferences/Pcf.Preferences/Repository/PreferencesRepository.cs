﻿using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Pcf.Preferences.Entities;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pcf.Preferences.Repository
{
    public class PreferencesRepository<T>: IPreferencesRepository<T> where T: BaseEntity
    {
        private readonly IDistributedCache _distributedCache;
        private readonly IConfiguration _configuration;
        private readonly ConnectionMultiplexer _redis;
        private readonly string[] _connection;

        public PreferencesRepository(IDistributedCache cache, IConfiguration configuration) {
            _distributedCache = cache ?? throw new ArgumentNullException(nameof(cache));
            _configuration = configuration;
            _redis = ConnectionMultiplexer.Connect(_configuration.GetConnectionString("Redis"));
            _connection = _redis.Configuration.Split(',')[0].Split(':');
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var keys = _redis.GetServer(_connection[0], int.Parse(_connection[1])).Keys();
            return keys.Select(key => JsonConvert.DeserializeObject<T>(_distributedCache.GetString(key))).ToList();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var preference = await _distributedCache.GetStringAsync(id.ToString());
            if (string.IsNullOrEmpty(preference))
                return null;
            return JsonConvert.DeserializeObject<T>(preference);
        }

        public async Task<T> UpdateAsync(T entity)
        {
            await _distributedCache.SetStringAsync(entity.Id.ToString(), JsonConvert.SerializeObject(entity));
            return await GetByIdAsync(entity.Id);
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var keys = _redis.GetServer(_connection[0], int.Parse(_connection[1])).Keys();
            return keys.Where(key => ids.Contains(Guid.Parse(key))).Select(key => JsonConvert.DeserializeObject<T>(_distributedCache.GetString(key))).ToList();
        }

        public async Task DeleteAsync(Guid id) => await _distributedCache.RemoveAsync(id.ToString());

        public async Task DeleteAllkeys()
        {
            var iServer = _redis.GetServer(_connection[0], int.Parse(_connection[1]));
            await iServer.FlushDatabaseAsync();
        }
    }
}
