﻿using Pcf.Preferences.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Pcf.Preferences.Repository
{
    public interface IPreferencesRepository<T> where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();

        Task<T> GetByIdAsync(Guid id);

        Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids);

        Task<T> UpdateAsync(T entity);

        Task DeleteAsync(Guid id);

        Task DeleteAllkeys();
    }
}
