﻿using Microsoft.AspNetCore.Mvc;
using Pcf.Preferences.Entities;
using Pcf.Preferences.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pcf.Preferences.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PreferencesController : ControllerBase
    {
        private readonly IPreferencesRepository<Preference> _repository;

        public PreferencesController(IPreferencesRepository<Preference> preferencesRepository) =>
            _repository = preferencesRepository ?? throw new ArgumentNullException(nameof(preferencesRepository));

        [HttpGet]
        public async Task<IEnumerable<Preference>> GetAsync()
        {
            return await _repository.GetAllAsync();
        }

        [HttpGet("GetRangeByIds")]
        public async Task<IEnumerable<Preference>> GetRangeByIdsAsync([FromQuery] List<Guid> ids)
        {
           return await _repository.GetRangeByIdsAsync(ids);
        }

        [HttpGet("GetById")]
        public async Task<Preference> GetByIdAsync([FromQuery] Guid id)
        {
            return await _repository.GetByIdAsync(id);
        }

        [HttpPost("UpdateKey")]
        public async Task<ActionResult<Preference>> UpdateAsync(Preference entity)
        {
            return Ok(await _repository.UpdateAsync(entity));
        }

        [HttpDelete("DeleteKey")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            await _repository.DeleteAsync(id);
            return Ok();
        }
        
        [HttpDelete("FlushAllKeys")]
        public async Task<IActionResult> DeleteAllkeys()
        {
            await _repository.DeleteAllkeys();
            return Ok();
        }
    }
}
